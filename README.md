# LearnGoWithTests
 
 以下のgolangチュートリアルを実施したリポジトリ

 https://quii.gitbook.io/learn-go-with-tests/
 
# hello
 
 
# integer
 

# iteration
 
 
# slices


# shapes
 
 
# wallet


# dictionary
 
 
# di

SpringBoot等とは違い引数に関数を入れることで疎結合にする

# countdown

diについてしっかり理解してないと難しい

# concurrency

所要時間　2h

学んだこと
* goroutinesで並列化を行う
* 無名関数で簡単に処理を書くことができる
* channelsを使うことで通信を制御

# racer

所要時間　2h

学んだこと
* selectは複数のチャネルを待機するのに利用
* time.Afterを使うことでタイムアウトを実装できる
* httptestを使うことでモックサーバを作成できる

# reflection
所要時間　2.5h

学んだこと
* reflectパッケージを利用することでany型を扱うことができるようになる
* reflectは多用すべきではない
* ネストされた構造は再帰を利用して処理を行う

# sync 
所要時間　1.5h

学んだこと
* MutexやWaitgroupで同期を行うことができる
* Mutexは複雑化しやすいので状態の管理のみに利用する
* go vetはエラーがでる「かもしれない」コードを表示してくれる
