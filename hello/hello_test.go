package main

import "testing"

func TestHello(t *testing.T) {

	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}

	t.Run("saying hello to people", func(t *testing.T) {
		got := Hello("Ryu", "")
		want := "Hello Ryu"
		assertCorrectMessage(t, got, want)
	})

	t.Run("say 'Hello World' when empty string is supplied", func(t *testing.T) {
		got := Hello("", "")
		want := "Hello World"
		assertCorrectMessage(t, got, want)
	})

	t.Run("in Spanish", func(t *testing.T) {
		got := Hello("Ryu", "Spanish")
		want := "Hola Ryu"
		assertCorrectMessage(t, got, want)
	})

	t.Run("in French", func(t *testing.T) {
		got := Hello("Ryu", "French")
		want := "Bonjour Ryu"
		assertCorrectMessage(t, got, want)
	})

}
