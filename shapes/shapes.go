package shapes

import "math"

//Shape 図形インターフェース
type Shape interface {
	Area() float64
}

//Rectangle 長方形構造体
type Rectangle struct {
	Width  float64
	Height float64
}

//Area 長方形面積算出メソッド
func (r Rectangle) Area() float64 {
	return r.Height * r.Width
}

//Circle 円構造体
type Circle struct {
	Radius float64
}

//Area 円面積算出メソッド
func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

//Triangle 三角形構造体
type Triangle struct {
	Base   float64
	Height float64
}

//Area 三角形面積算出メソッド
func (t Triangle) Area() float64 {
	return t.Base * t.Height * 0.5
}

//Perimeter 周算出メソッド
func Perimeter(rectangle Rectangle) float64 {
	return 2 * (rectangle.Width + rectangle.Height)
}

//Area 面積算出メソッド
func Area(rectangle Rectangle) float64 {
	return rectangle.Width * rectangle.Height
}
