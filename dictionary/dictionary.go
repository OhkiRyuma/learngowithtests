package dictionary

const (
	//ErrNotFound 単語が見つからなかった際のエラー
	ErrNotFound = DictionaryErr("could not find the word you were looking for")
	//ErrWordExists 既に存在する単語を追加した際のエラー
	ErrWordExists = DictionaryErr("cannot add word because it already exists")
	//ErrWordDoesNotExist 存在しない単語を変更しようとした際のエラー
	ErrWordDoesNotExist = DictionaryErr("cannot update word because it does not exist")
)

//Dictionary 単語を保存する構造体
type Dictionary map[string]string

//DictionaryErr Dictionaryに関するエラー
type DictionaryErr string

//Search 単語検索メソッド
func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]
	if !ok {
		return "", ErrNotFound
	}
	return definition, nil
}

//Add 単語追加メソッド
func (d Dictionary) Add(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		d[word] = definition
	case nil:
		return ErrWordExists
	default:
		return err
	}

	return nil
}

//Update 単語更新メソッド
func (d Dictionary) Update(word, definition string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		return ErrWordDoesNotExist
	case nil:
		d[word] = definition
	default:
		return err
	}

	return nil
}

//Delete 単語削除メソッド
func (d Dictionary) Delete(word string) {
	delete(d, word)
}

func (e DictionaryErr) Error() string {
	return string(e)
}
