package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

const finalWord = "Go!"
const countdownStart = 3

//Sleeper スリープインターフェース
type Sleeper interface {
	Sleep()
}

func main() {
	sleeper := &ConfigurableSleeper{1 * time.Second, time.Sleep}
	Countdown(os.Stdout, sleeper)
}

//Countdown カウントダウンメソッド
func Countdown(out io.Writer, sleeper Sleeper) {

	for i := countdownStart; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(out, i)
	}
	sleeper.Sleep()
	fmt.Fprint(out, finalWord)
}

//ConfigurableSleeper スリープ時間の設定可能なスリープメソッド
type ConfigurableSleeper struct {
	durration time.Duration
	sleep     func(time.Duration)
}

//Sleep スリープメソッド
func (c *ConfigurableSleeper) Sleep() {
	c.sleep(c.durration)
}
