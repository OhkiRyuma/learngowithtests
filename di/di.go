package main

import (
	"fmt"
	"io"
	"net/http"
)

//Greet 挨拶メソッド
func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello %s", name)
}

//MyGreeterHandler HTTPを使った挨拶ハンドラー
func MyGreeterHandler(w http.ResponseWriter, r *http.Request) {
	Greet(w, "world")
}

func main() {
	http.ListenAndServe(":5000", http.HandlerFunc(MyGreeterHandler))
}
