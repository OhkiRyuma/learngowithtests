package wallet

import (
	"errors"
	"fmt"
)

//ErrInsufficientFunds 支払いに必要なBitCoinが足りない際のエラー
var ErrInsufficientFunds = errors.New("cannot withdraw, insufficient funds")

//BitCoin 仮想通貨単位
type BitCoin int

//Wallet BitCoinを保存する構造体
type Wallet struct {
	balance BitCoin
}

func (b BitCoin) String() string {
	return fmt.Sprintf("%d BTC", b)
}

//Deposit BitCoin預金メソッド
func (w *Wallet) Deposit(amount BitCoin) {
	w.balance += amount
}

//Withdraw BitCoin支払いメソッド
func (w *Wallet) Withdraw(amount BitCoin) error {
	if amount > w.balance {
		return ErrInsufficientFunds
	}

	w.balance -= amount
	return nil
}

//Balance 保有BitCoin表示メソッド
func (w *Wallet) Balance() BitCoin {
	return w.balance
}
